#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */
void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
	// do not use increments, use two different binary search loop
    indexMin = indexMax = -1; 		//cas dans lequel la valeur n'est pas dans le tableau
	//On reprend l'algorithme précédent deux fois pour indexMin et indexMax.

	int start = 0;
	int end = array.size();
	int mid;
	while(start<end){
		mid = (start + end)/2;
		if(toSearch > array[mid]){
			start = mid + 1;
		}
		else if(toSearch < array[mid]){
			end = mid;
		}
		else{
			indexMin = mid;
			//on peut pas retourne mid car void function.
			//return mid; 		//si index minimal est mid on arrive à la fin
			end = mid;
		}
	}

	 start = 0; 
	 end = array.size();
	while(start<end){
		mid = (start + end)/2;
		if(toSearch > array[mid]){
			start = mid + 1;
		}
		else if(toSearch < array[mid]){
			end = mid;
		}
		else{
			indexMax = mid;
			//return mid + 1; 	//si indexMax est mid on itere jusqu a arriver a end.
			start = mid + 1;
		}
	}

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
