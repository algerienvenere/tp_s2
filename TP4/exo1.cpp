#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
	*this[i] = value;
	while(i>0 && *this[i]>*this[(i-1)/2]){
		this-swap(i,(i-1)/2);
		i = (i-1)/2;
	}

}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
	int i = nodeIndex;

	    if (this->leftChild(i) < heapSize)
    {
        if (*this[i_max] < *this[this->leftChild(i)])
            i_max = this->leftChild(i);
    }

    if (this->rightChild(i) < heapSize)
    {
        if (*this[i_max] < *this[this->rightChild(i)])
            i_max = this->rightChild(i);
    }

    if (i_max != i)
    {
        this->swap(i, i_max);
        this->heapify(heapSize, i_max);
    }

}

void Heap::buildHeap(Array& numbers)
{
	 for (size_t i = 0; i < numbers.size(); i++)
    {
        this -> insertHeapNode(numbers.size(), numbers[i]);
    }

}

void Heap::heapSort()
{
	for (size_t i = this->size()-1 ; i > 0 ; i--)
    {
        this->swap(0, i);
        this->heapify(i, 0);
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
